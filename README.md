# Unique combinations generator:

Usage:
```
>>> generate_combinations(options_list, list_length)
Return list with all possible combinations of options(options_list) with length(list_length) of combination
```
Example:
```
>>> generate_combinations([0,1],3)
[[0, 0, 0], [0, 0, 1], [0, 1, 0], [0, 1, 1], [1, 0, 0], [1, 0, 1], [1, 1, 0], [1, 1, 1]]

>>> generate_combinations(["r","g","b"],2)
[['r', 'r'], ['r', 'g'], ['r', 'b'], ['g', 'r'], ['g', 'g'], ['g', 'b'], ['b', 'r'], ['b', 'g'], ['b', 'b']]
```