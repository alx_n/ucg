#!/usr/bin/env python
# coding=utf-8


def __ten_to_listed_convert(options, list_length, n):
    if n == 0:
        return [options[0]]* list_length
    nums = []
    while n:
        n, r = divmod(n, len(options))
        nums.append(options[r])
    while len(nums) < list_length:
        nums.append(options[0])
    return [l for l in reversed(nums)]


def generate_combinations(options_list, list_length):

    variants = []

    for i in range(len(options_list) ** list_length):
        variants.append(__ten_to_listed_convert(options_list, list_length, i))

    return variants